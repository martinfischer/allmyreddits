# -*- coding: utf-8 -*-
"""
allmyreddits.py - a greasemonkey script generator for reddit

This programm was written for Python 2.7.1 and should work on multiple platforms. 
Tested on Ubuntu 11.4 and Windows 7. 

@author: Martin Rupert Fischer @ https://plus.google.com/113711733559138753519/about
@copyright: (c) 2011 Martin Rupert Fischer
@license: MIT, see MIT-LICENSE for more details.
"""
import urllib2
import urllib
import time
import json
from Tkinter import *
from tkFileDialog import askdirectory
import os
# Version is formated into various strings within this programm
version = "1.0"
# Top greasemonkey template
template1 = """\
// ==UserScript==
// @name            allmyreddits
// @version         {}
// @description     This will place links to all your frontpaged subreddits in a box in the sidebar
// @author          Martin Rupert Fischer - https://plus.google.com/113711733559138753519/about
// @include         http://www.reddit.com/*
// ==/UserScript==

var script = document.createElement("script"); 
script.type = "text/javascript";
""".format(version)
# Bottom greasemonkey template
template2 = """
document.getElementsByTagName("head")[0].appendChild(script);
"""

class Application(Frame):
    """
    The only class this programm has. 
    
    It sets up the gui and the methods do the work.
    """
    # Setting up and empty string and an empty list to be filled later
    j = ""
    reddits = []
    # Setting up the urllib2 opener and append a user-agent
    opener = urllib2.build_opener()
    opener.addheaders.append(("User-Agent", "allmyreddits - greasemonkey script generator v{}".format(version)))
    def __init__(self, master=None):
        # Setting up the gui
        Frame.__init__(self, master)
        root.title("allmyreddits - greasemonkey script generator v{}".format(version))
        self.grid(sticky=W+E+N+S)
        # The top text label
        self.v1 = StringVar()
        self.lab1_amr = Label(self, height=3, textvariable=self.v1)
        self.v1.set("Fill in your Reddit username and password.")
        self.lab1_amr.grid(row=0, column=1)
        # The username label
        self.utit_amr = Label(self, text="Username:")
        self.utit_amr.grid(row=1, column=1)
        # The username entry field
        self.user_amr = Entry(self, width=30,)
        self.user_amr.grid(row=2, column=1)
        self.user_amr.focus_set()
        # The password label
        self.ptit_amr = Label(self, text="Password:")
        self.ptit_amr.grid(row=3, column=1)
        # The password entry field
        self.pass_amr = Entry(self, width=30, show="*")
        self.pass_amr.grid(row=4, column=1)
        # The second text label   
        self.v2 = StringVar()
        self.lab2_amr = Label(self, height=4, textvariable=self.v2)
        self.v2.set("Then click on \"Get all my Reddits\".")
        self.lab2_amr.grid(row=5, column=1)
        # The save button
        self.save_amr = Button(self, text="Save", width=10 , height=2, relief=GROOVE, state=DISABLED, command=self.save_reddits)
        self.save_amr.grid(row=6, column=0)
        # The get all my reddits button
        self.get_amr = Button(self, text="Get all my Reddits", width=45, height=2, relief=GROOVE, command=self.get_reddits)
        self.get_amr.grid(row=6, column=1)
        # The quit button
        self.quit_amr = Button(self, text="Quit", width=10 , height=2, relief=GROOVE, command=self.quit)
        self.quit_amr.grid(row=6, column=2)
        # Get the size of the window and disable resizing
        root.update()
        root.minsize(root.winfo_width(), root.winfo_height())
        root.maxsize(root.winfo_width(), root.winfo_height())

    def get_reddits(self):
        """
        This method first tries to login, if this fails the top text label 
        will show the error message.
        
        After loging in and getting the session cookie, the subrscribed 
        subreddits of the loged in account are fetched
        
        All this is done over Reddits json API.
        
        After successful fetching of the subreddit information this function
        prepares a greasemonkey script.
        """
        # Getting username and password and striping whitespace
        self.username = self.user_amr.get().strip()
        self.password = self.pass_amr.get().strip()
        # Validate
        if self.username != "" and self.password != "":
            self.v1.set("Loging in ...")
            self.v2.set("This could take a little while.\nGet some bacon while I fetch your Reddits.")
            self.get_amr.config(state=DISABLED)
            self.user_amr.config(state=DISABLED)
            self.pass_amr.config(state=DISABLED)
            self.update()
            # Login
            url = "http://www.reddit.com/api/login/{}".format(self.username)
            values = {"user":self.username, "passwd":self.password, "api_type":"json"}
            values = urllib.urlencode(values)
            req = self.opener.open(url, values)
            response = req.read()
            self.j = json.loads(response)
            # Parse login response
            if not self.j["json"]["errors"]:
                self.v1.set("Fetching ...")
                self.update()
                time.sleep(2)
                # Append cookie to the opener
                cookie = self.j["json"]["data"]["cookie"]
                self.opener.addheaders.append(("Cookie", "reddit_session={}".format(cookie))) 
                # Fetching subreddits in a while loop
                after = None
                while True:
                    req1 = self.opener.open("http://www.reddit.com/reddits/mine.json?after={}".format(after))
                    p1 = req1.read()
                    j1 = json.loads(p1)
                    self.update()
                    # Respect API rate limit
                    time.sleep(2)
                    # Parse subreddit links, names and append to reddits list
                    for i in range(len(j1["data"]["children"])): 
                        url = "http://www.reddit.com" + j1["data"]["children"][i]["data"]["url"].encode() 
                        name = url.split("/")[-2].lower()
                        tup =  name, url
                        self.reddits.append(tup)
                    # Check for "after", keep going if present, break if not
                    try:
                        after = j1["data"]["after"].encode()
                    except AttributeError:
                        break
                # Check if reddits list is empty or full of subreddits
                if len(self.reddits) == 0:
                    self.v1.set("No reddits found ...")
                    self.v2.set("For some reason nothing was found.\nTry again?")
                    self.update()
                else:
                    self.v1.set("Preparing ...")
                    self.update()
                    self.reddits.sort()
                    # Preparing the greasemonkey script using templates
                    linklist = """"""
                    for name, link in self.reddits:
                        linklist+="""<li><a href=\\"{}\\">{}</a><li>""".format(link,name)
                    # jQuery, CSS and links template, links are formated in   
                    jq = """script.innerHTML = "$(function(){{$('<div style=\\"float:right;width:278px;height:235px;margin:7px 0 12px 0;padding:5px 10px;font-size:11px;line-height:15px;overflow:auto;clear:both;background-color:#EFF7FF;border:1px solid grey;\\"><ul>{}</ul></div>').insertBefore('.side > :first-child');}});";""".format(linklist)     
                    # Putting the script together
                    self.str = template1 + jq + template2            
                    # Prepare to save
                    self.get_amr.config(state=NORMAL)
                    self.user_amr.config(state=NORMAL)
                    self.pass_amr.config(state=NORMAL)
                    self.save_amr.config(state=NORMAL)
                    self.get_amr.config(state=DISABLED)
                    self.v1.set("Done.")
                    self.v2.set("Now click on \"Save\", and choose a directory.")
                    self.update()            

            else:
                # Show error response from the login
                self.v1.set("Response error: {}\n{}".format(self.j["json"]["errors"][0][0],self.j["json"]["errors"][0][1]))
                self.v2.set("Something went wrong at the login.")       
                self.get_amr.config(state=NORMAL)
                self.user_amr.config(state=NORMAL)
                self.pass_amr.config(state=NORMAL)
                time.sleep(2)
                self.update()
        else:
            # Keeps naging as long as both username and password are empty or filled with whitespace
            self.v2.set("Please fill in your username and password.\n\nAfter that click on \"Get all my Reddits\".")
            self.update()

    def save_reddits(self):
        """
        Ask for a directory to save the script.
        
        Saves a file called "allmyreddits.user.js" in the directory.
        """        
        dir = askdirectory()
        # os.path.join makes this work on several platforms
        path = os.path.join(dir, "allmyreddits.user.js")
        file = open(path, "w")
        file.write(self.str)
        file.close()
        self.v1.set("Saved as \"allmyreddits.user.js\" in the folder you chose.")
        self.v2.set("Now drag that file into your browser.\nThen browse Reddit, you'll find a new box.")
        self.update()

if __name__=="__main__":
    root = Tk()
    app = Application(master=root)
    app.mainloop()
